import React, { Component } from 'react';
import styled from 'styled-components';
import './App.css';
import FontApp from './components/FontApp';

class App extends Component {
  render() {
    return (
      <AppContainer className="App">
        {/* <FontApp /> */}
        <FontApp />
      </AppContainer>
    );
  }
}

const AppContainer = styled.div`
  height: 90vh;
  display: flex;
  flex-direction: column;
  margin: 16px;
`;

export default App;
