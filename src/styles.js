export const fontFamilies = {
  openSans: 'Open Sans',
  system: 'system-ui',
  Oswald: 'Oswald',
  IndieFlower: 'IndieFlower',
  YatraOne: 'YatraOne',
  Cryptography: 'Cryptography',
  Iconography: 'Iconography',
  Typography: 'Typography'
};

export const colors = {
  black: '#333',
  green: '#060'
};
