import React, { PureComponent } from 'react';
import styled from 'styled-components';

import { fontFamilies } from '../styles';

const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  top: 0;
  position: fixed;
  background-color: none;
  width: 100%;
  z-index: 1;
  padding: 1em 0em;
`;

const Nav = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-right: 3em;
`;

const Typo = styled.a`
  font-family: ${fontFamilies.Typography};
  font-size: 3em;
  margin: 0px 8px;
  cursor: pointer;
  :visited {
    color: #000;
  }
  text-decoration: none;
  ::selection {
    color: #00f;
    background-color: none;
  }
`;

const Title = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 3em;
`;

const TitleTop = styled.div`
  display: flex;
  flex-direction: row;
`;

const Link = styled.a`
  font-family: ${fontFamilies.Typography};
  font-size: 3em;
  margin: 0em 0.5em;
  cursor: pointer;

  ::selection {
    color: #00f;
    background-color: none;
  }
`;

class Header extends PureComponent {
  // componentDidMount() {
  //   document.addEventListener('scroll', this.handleScroll);
  //   this.header = document.getElementById("myHeader");
  // }

  // componentWillUnmount() {
  //   document.removeEventListener('scroll');

  // }

  // handleScroll = (event) => {
  //   if (window.pageYOffset >= sticky) {
  //     this.header.classList.add("sticky");
  //   } else {
  //     this.header.classList.remove("sticky");
  //   }
  // }

  render() {
    const navs = [
      { id: 'test', value: 'Test' },
      { id: 'about', value: 'About' },
      { id: 'click', value: 'Specimen' }
    ];
    const loc = window.location;
    return (
      <HeaderContainer id="header">
        <Title>
          <TitleTop>
            <Typo href={loc}>Short Type</Typo>
          </TitleTop>
        </Title>
        <Nav>
          {navs.map(n => (
            <div key={n.id}>
              <Link
                onClick={() => {
                  var elmnt = document.getElementById(n.id);
                  elmnt.scrollIntoView();
                  window.scrollBy(0, -50);
                }}
              >
                {n.value}
              </Link>
            </div>
          ))}
        </Nav>
      </HeaderContainer>
    );
  }
}

export default Header;
