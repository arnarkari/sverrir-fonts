import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { append, contains, reject, equals } from 'ramda';

class FontSelect extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { values: props.selectedFonts };
  }

  remove = value => {
    const newValues = reject(equals(value), this.state.values);
    this.setState({ values: newValues });
    this.props.onFontSelect(newValues);
  };

  add = value => {
    const newValues = append(value, this.state.values);
    this.setState({ values: newValues });
    this.props.onFontSelect(newValues);
  };

  onClick = value => {
    if (this.contains(value)) {
      this.remove(value);
    } else {
      this.add(value);
    }
  };

  contains = value => contains(value, this.state.values);

  renderCheckbox = font => {
    return (
      <FontLabel fontFamily={font} key={font}>
        <input
          type="checkbox"
          onClick={() => this.onClick(font)}
          defaultChecked={this.contains(font)}
        />
        {font}
      </FontLabel>
    );
  };

  render() {
    return <FontForm>{this.props.fonts.map(this.renderCheckbox)}</FontForm>;
  }
}

const FontLabel = styled.label`
  font-family: ${props => props.fontFamily};
  font-size: 28px;
`;

const FontForm = styled.form`
  display: flex;
  flex-direction: column;
`;

export default FontSelect;
