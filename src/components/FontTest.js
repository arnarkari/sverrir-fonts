import React, { PureComponent } from 'react';
import styled from 'styled-components';
import Rx from 'rxjs/Rx';

import '../App.css';
import { fontFamilies } from '../styles';
import tryButtonImg from '../images/try.svg';

class FontTest extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { testText: '' };
  }

  componentDidMount() {
    this.createObservable('Try it now!', 1000);
  }

  componentWillUnmount() {
    if (this.subscription) {
      this.subscription.unsubscribe && this.subscription.unsubscribe();
    }
  }

  createObservable(sentence, firstDelay) {
    const obs = Rx.Observable.zip(
      Rx.Observable.from(sentence.split('')),
      Rx.Observable.timer(firstDelay, 120),
      (value, timer) => value
    ).do(x => {
      this.setState(prevState => ({ testText: prevState.testText + x }));
    });
    // .zip(Rx.Observable.timer(5000, 50), (value, timer) => value)
    // .do(x =>
    //   this.setState(prevState => ({ value: prevState.value.slice(0, -1) }))
    // )
    // .repeat();
    this.subscription = obs.subscribe(x => {});
  }

  handleChange = event => {
    this.setState({ testText: event.target.value });
  };

  handleOnClick = () => {
    if (this.subscription.unsubscribe) {
      this.subscription.unsubscribe();
    }
    this.setState({ testText: '' }, () =>
      this.createObservable('Write here!', 0)
    );
  };

  render() {
    return (
      <Container id="test">
        <ClickButton src={tryButtonImg} onClick={this.handleOnClick} />
        <FontInput
          type="text"
          fontFamily={fontFamilies.Typography}
          value={this.state.testText}
          onChange={this.handleChange}
          maxLength={16}
          className="customCursor"
        />
        <FontInput
          type="text"
          fontFamily={fontFamilies.Iconography}
          value={this.state.testText}
          onChange={this.handleChange}
          maxLength={16}
          className="customCursor"
        />
        <FontInput
          type="text"
          fontFamily={fontFamilies.Cryptography}
          value={this.state.testText}
          onChange={this.handleChange}
          maxLength={16}
          className="customCursor"
        />
      </Container>
    );
  }
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  position: relative;
`;

const ClickButton = styled.img`
  position: absolute;
  height: 350px;
  left: 1.7em;
  top: -5em;
  transition: transform 0.3s;

  :hover {
    transform: rotate(180deg);
  }

  ::selection {
    background-color: none;
  }
`;

const FontInput = styled.input.attrs({
  spellCheck: 'false'
})`
  text-align: center;
  width: 100%;
  -webkit-appearance: none;
  border: none;
  font-family: ${props => props.fontFamily};
  font-size: 13em;
  height: 1.3em;
  :focus {
    outline: none;
  }

  ::selection {
    color: #00f;
    background-color: #fff;
  }
`;

export default FontTest;
