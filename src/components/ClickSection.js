import React, { PureComponent } from 'react';
import styled from 'styled-components';

import clickButtonImg from '../images/click.svg';
import { fontFamilies } from '../styles';

const ALPHABET = 'aábcdðeéfghiíjklmnoópqrstuvwxyýzþæö';

class ClickSection extends PureComponent {
  constructor() {
    super();
    this.state = {
      index: 0
    };
  }

  handleOnClick = () => {
    if (this.state.index === 34) {
      this.setState({ index: 0 });
    } else {
      this.setState(prevState => ({ index: prevState.index + 1 }));
    }
  };

  render() {
    return (
      <Container onClick={this.handleOnClick} id="click">
        <ClickButton src={clickButtonImg} />
        <Letter fontFamily={fontFamilies.Typography}>
          {ALPHABET[this.state.index]}
        </Letter>
        <Letter fontFamily={fontFamilies.Iconography}>
          {ALPHABET[this.state.index]}
        </Letter>
        <Letter fontFamily={fontFamilies.Cryptography}>
          {ALPHABET[this.state.index]}
        </Letter>
      </Container>
    );
  }
}

const ClickButton = styled.img`
  position: absolute;
  height: 450px;
  right: 2em;
  transition: transform 0.3s;

  :hover {
    transform: rotate(-180deg);
  }

  ::selection {
    background-color: none;
  }
`;

const Container = styled.div`
  position: relative;
  min-height: 100vh;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-content: center;
  cursor: pointer;
`;

const Letter = styled.div`
  font-family: ${props => props.fontFamily};
  font-size: 20em;
  margin: 0em 1em;
  user-select: none;
`;

export default ClickSection;
