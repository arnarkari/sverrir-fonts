import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { fontFamilies } from '../styles';

import FontSelfDisplay from './FontSelfDisplay';
const WRITING_BY_SOUND = 'Writing by Sound';

const Container = styled.div`
  min-height: 70vh;
  display: flex;
  margin: 0em 10em;
  position: relative;
`;

const AbsoluteContainer = styled.div`
  font-family: ${fontFamilies.Typography};
  position: absolute;
`;

class ColorSection extends PureComponent {
  render() {
    return (
      <Container>
        <AbsoluteContainer>
          <FontSelfDisplay
            typeOut={WRITING_BY_SOUND}
            fontFamily={fontFamilies.Typography}
            height={140}
            delay={800} // 800
            typeSpeed={120}
            repeatDelay={9360}
          />
        </AbsoluteContainer>
        <AbsoluteContainer>
          <FontSelfDisplay
            typeOut={WRITING_BY_SOUND}
            fontFamily={fontFamilies.Iconography}
            height={140}
            delay={3720} // 2720
            typeSpeed={120}
            repeatDelay={9360}
            color={'#00f'}
          />
        </AbsoluteContainer>
        <AbsoluteContainer>
          <FontSelfDisplay
            typeOut={WRITING_BY_SOUND}
            fontFamily={fontFamilies.Cryptography}
            height={180}
            delay={6640} // 3320
            typeSpeed={120}
            repeatDelay={9360}
            color={'#0f0'}
          />
        </AbsoluteContainer>
      </Container>
    );
  }
}

export default ColorSection;
