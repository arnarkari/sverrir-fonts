import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { fontFamilies } from '../styles';

import islFlag from '../images/isl.svg';
import engFlag from '../images/eng.svg';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
  max-width: 100vw;
`;

const AboutText = styled.div`
  font-family: ${fontFamilies.Typography};
  font-size: 3em;
  margin: 0em 8em;
  line-height: 1.3;

  ::selection {
    color: #00f;
    background-color: none;
  }
`;

const LanguageSwitcher = styled.div`
  display: flex;
  flex-direction: column;
  align-self: flex-end;
`;

const LanguageButton = styled.a`
  position: relative;
  display: flex;
  flex-direction: row;
  cursor: pointer;
  font-family: ${fontFamilies.Typography};
  font-size: 3em;
  margin: 0.2em 1em;
  transition: transform 1s;

  ::selection {
    color: #00f;
    background-color: none;
  }
`;

const Flag = styled.img`
  position: absolute;
  left: 10%;
  top: 10%;
  background-color: white;
`;

class About extends PureComponent {
  constructor() {
    super();
    this.state = {
      showIcelandic: true
    };
  }

  render() {
    return (
      <Container id="about">
        <LanguageSwitcher>
          <LanguageButton
            onClick={() => this.setState({ showIcelandic: true })}
          >
            {this.state.showIcelandic ? (
              <Flag src={islFlag} height={50} />
            ) : null}{' '}
            Íslenska
          </LanguageButton>
          <LanguageButton
            onClick={() => this.setState({ showIcelandic: false })}
          >
            {!this.state.showIcelandic ? (
              <Flag src={engFlag} height={50} />
            ) : null}{' '}
            English
          </LanguageButton>
        </LanguageSwitcher>
        {this.state.showIcelandic ? (
          <AboutText>
            Letrið Short Type er lauslega byggt á hraðritunarkerfi Isaac Pitmans
            frá árinu 1837, og eru tengingar og endurtekin form áberandi. Letrið
            er hannað sem leturfjölskylda: Stenography, Typography og
            Cryptography. Stenography er skyldast áðurnefndu hraðritunarkerfi,
            en minnir um leið á til dæmis georgísk og armenísk stafróf.
            Typography-útgáfuna má sjá sem „venjulegt“ letur en það er einfölduð
            og aðgengilegri útgáfa af Stenography. Það síðast nefnda -
            Cryptography - er dulmálsletur sem byggir á hinum tveimur en hér eru
            formin sem mynda hvern staf tekin í sundur eftir ákveðinni reglu.
          </AboutText>
        ) : (
          <AboutText>
            The Short Type typeface is broadly based on Isaac Pitman's shorthand
            system of 1837, and prominently displays ligatures and repeated
            forms. The typeface is designed as a family: Stenography, Typography
            and Cryptography. Stenography relates most closely to shorthand, but
            also recalls, for example, the Georgian and Armenian alphabets. The
            Typography version may be seen as a "normal" typeface, being a
            simplified and more accessible version of Stenography. The last -
            Cryptography - is, as the name implies, a set of cryptographic
            lettering, in which the forms that make up each letter are
            systematically deconstructed.
          </AboutText>
        )}
      </Container>
    );
  }
}

export default About;
